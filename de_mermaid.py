import inkex

class DeMermaid(inkex.EffectExtension):

    def add_arguments(self, pars):
        pass
    
    def effect(self):

        fos = self.svg.xpath('//svg:foreignObject')

        for _fo in fos:
            _fo.getparent().remove(_fo)

        all_elements = self.svg.xpath('//svg:*')

        for element in all_elements:

            if 'class' in element.attrib.keys():
                element.pop('class')
        
if __name__ == '__main__':
    DeMermaid().run()
